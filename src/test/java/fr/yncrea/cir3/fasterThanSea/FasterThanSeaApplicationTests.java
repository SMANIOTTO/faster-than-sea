package fr.yncrea.cir3.fasterThanSea;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.yncrea.cir3.fasterThanSea.domain.Boat;
import fr.yncrea.cir3.fasterThanSea.domain.Game;
import fr.yncrea.cir3.fasterThanSea.domain.Player;
import fr.yncrea.cir3.fasterThanSea.domain.Square;
import fr.yncrea.cir3.fasterThanSea.domain.Weapon;
import fr.yncrea.cir3.fasterThanSea.form.CreationGameForm;
import fr.yncrea.cir3.fasterThanSea.repository.BoatRepository;
import fr.yncrea.cir3.fasterThanSea.repository.GameRepository;
import fr.yncrea.cir3.fasterThanSea.repository.PlayerRepository;
import fr.yncrea.cir3.fasterThanSea.repository.SquareRepository;
import fr.yncrea.cir3.fasterThanSea.repository.WeaponRepository;
import fr.yncrea.cir3.fasterThanSea.service.GenerationService;
import fr.yncrea.cir3.fasterThanSea.service.ResetService;

@SpringBootTest
class FasterThanSeaApplicationTests {
	
	@Autowired
	private GenerationService generationService;
	
	@Autowired
	private GameRepository gamesInDB;
	
	@Autowired
	private PlayerRepository playersInDB;
	
	@Autowired
	private SquareRepository squaresInDb;
	
	@Autowired
	private BoatRepository boatsInDb;
	
	@Autowired
	private WeaponRepository weaponsInDb;
	
	

	@Test
	@Order(1)
	void testCreateGame() {
		
		CreationGameForm creationGameForm = new CreationGameForm();
		
		//1) Fill test form
		String[] boatsName = new String[5];
		boatsName[0] = "boat1";
		boatsName[1] = "boat2";
		boatsName[2] = "boat3";
		boatsName[3] = "boat4";
		boatsName[4] = "boat5";
		creationGameForm.setBoatsName(boatsName);
		creationGameForm.setGameName("gameName");
		creationGameForm.setId((long)1);
		creationGameForm.setPlayerName("playerName");
		
		//2) Test function
		generationService.createGame(creationGameForm);
		
		//3) Result
		//GAME
		Game gameToTest = gamesInDB.findCurrentGame();
		assertEquals(gameToTest.getName(), "gameName");
		assertEquals(gameToTest.getIsFinish(), false);
		
		//PLAYER
		Player playerToTest = playersInDB.findCurrentPlayer();
		assertEquals(playerToTest.getInit(), true);
		assertEquals(playerToTest.getName(), "playerName");
		
		//BOAT
		int i=1;
		for(Boat boatToTest : boatsInDb.findBoatsCrescentByPlayerId(playerToTest.getId())) {
			assertEquals(boatToTest.getIsPlaced(), false);
			assertEquals(boatToTest.getName(), "boat" + i);
			i++;
		}
		
		//SQUARE
		for(Square squareToTest : squaresInDb.findSquaresCrescentByPlayerId(playerToTest.getId())) {
			assertEquals(squareToTest.getState(), 'e');
		}
		
	}
	
	
	
	@Test
	@Order(2)
	void testWeaponCreation() {
		
		//1) test function
		generationService.createWeapons();
		
		//2) Results
		List<Weapon> weaponsToTest = weaponsInDb.findWeaponCrescent();
		int[] damage = {2, 4, 3};
		String[] effect = {"line", "area", "random"};
		String[] name = {"torpedo", "bomb", "mortar"};
		int[] size = {2, 5, 3};
		String[] description = {"Fire on line of 2 cases", "Fire on area of 2x2 cases", "Fire on random 3 cases"};
		for(int i=0; i<3; i++) {
			assertEquals(weaponsToTest.get(i).getDamage(), damage[i]);
			assertEquals(weaponsToTest.get(i).getEffect(), effect[i]);
			assertEquals(weaponsToTest.get(i).getName(), name[i]);
			assertEquals(weaponsToTest.get(i).getSize(), size[i]);
			assertEquals(weaponsToTest.get(i).getDescription(), description[i]);
		}
		
	}
	
	
	
	/*@Test
	void testEnnemyCreation() {
		
		//1) Init default settings
		Player ennemyToTest = playersInDB.findCurrentEnnemy();
		List<Square> ennemySquares = squaresInDb.findSquaresCrescentByPlayerId(playersInDB.findCurrentEnnemy().getId());
		List<Boat> ennemyBoats = boatsInDb.findBoatsCrescentByPlayerId(playersInDB.findCurrentEnnemy().getId());
		String[] names = {"Aircraft carrier", "Cruiser", "Destroyer", "Submarine", "Torpedo"};
		
		//2) Test function
		generationService.createEnnemy(ennemyToTest, ennemySquares, ennemyBoats);
		
		//3) Results
		assertEquals(playersInDB.findCurrentEnnemy().getName(), "ennemy");
		assertEquals(playersInDB.findCurrentEnnemy().getInit(), false);
		
		//BOAT
		int i=1;
		for(Boat boatToTest : boatsInDb.findBoatsCrescentByPlayerId(ennemyToTest.getId())) {
			assertEquals(boatToTest.getIsPlaced(), true);
			assertEquals(boatToTest.getName(), names[i]);
			i++;
		}
		
		//SQUARE
		for(Square squareToTest : squaresInDb.findSquaresCrescentByPlayerId(ennemyToTest.getId())) {
			assertEquals(squareToTest.getState(), 'e');
		}
	}*/

}
