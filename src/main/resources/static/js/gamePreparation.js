function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(currentElement, ev) {
	ev.preventDefault();
  	var data = ev.dataTransfer.getData("text");
  	
  	var info = currentElement.id.split(',');
	
	//1) Get the ship size and load the page for sending to DB
	var defaultSize = [5, 4, 3, 3, 2];
	if(info[2] != -1){
		
		//1.1) Check if ship selected
		var locate = window.location.toString().split('/');
		if(locate[5] != null && locate[6] != null){//SHIP SELECTED
		
			//1.1.1) Build new URL
			var url = "";
			for(let i=0; i<7; i++){
				url = url + locate[i] + '/';
			}
			window.location.assign(url + info[0] + '/' + info[1]);
		}
		else{//SHIP NOT SELECTED
			alert("Please, choose a ship first");
		}
		
	}
	else{
		alert("All the ships were placed, reset or start the game !");
	}
}