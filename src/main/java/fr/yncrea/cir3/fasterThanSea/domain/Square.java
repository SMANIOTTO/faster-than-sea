package fr.yncrea.cir3.fasterThanSea.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="square")
public class Square {
	
	//COLUMN//
	@Id @Column
	@GeneratedValue(generator = "seqSquare")
	@SequenceGenerator(name = "seqSquare", sequenceName = "seq_square" ,allocationSize=1 )
	private Long id;
	
	@Column(nullable=false)
	private int x;
	
	@Column(nullable=false)
	private int y;
	
	@Column
	private Character state;

	@ManyToOne(cascade=CascadeType.PERSIST)
	private Boat boat;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	private Player player;
	

	//GETTER & SETTER//
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Character getState() {
		return state;
	}

	public void setState(Character state) {
		this.state = state;
	}
	
	public Boat getBoat() {
		return boat;
	}

	public void setBoat(Boat boat) {
		this.boat = boat;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
}	