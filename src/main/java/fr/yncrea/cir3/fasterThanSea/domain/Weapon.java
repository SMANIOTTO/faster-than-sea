package fr.yncrea.cir3.fasterThanSea.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="weapon")
public class Weapon {
	
	//COLUMN//
	@Id @Column
	@GeneratedValue(generator = "seqWeapon")
	@SequenceGenerator(name = "seqWeapon", sequenceName = "seq_weapon", allocationSize=1)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private int damage;
	
	@Column
	private String effect;
	
	@Column
	private int size;
	
	@ManyToOne
	private Store store;
	
	@Column
	private String description;
	
	@Column
	private boolean isBuy;
	
	@Column
	private boolean isActive;
	
	@OneToOne
	private Boat boat;



	//GETTER & SETTER//
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean getIsBuy() {
		return isBuy;
	}

	public void setBuy(boolean isBuy) {
		this.isBuy = isBuy;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Boat getBoat() {
		return boat;
	}

	public void setBoat(Boat boat) {
		this.boat = boat;
	}
}