package fr.yncrea.cir3.fasterThanSea.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="player")
public class Player {

	//COLUMN//
	@Id @Column
	@GeneratedValue(generator = "seqPlayer")
	@SequenceGenerator(name = "seqPlayer", sequenceName = "seq_player", allocationSize=1)
	private Long id;
	
	@ManyToMany
	private List<Game> games = new ArrayList<>();
	
	@OneToMany(mappedBy="player",cascade=CascadeType.ALL,orphanRemoval = true)
	private List<Boat> boats;
	
	@Column
	private String name;
	
	@Column
	private boolean isUser;
	
	@Column(nullable = false)
	private boolean isInit;
	
	@Column
	private boolean isCurrent;
	
	@OneToMany(mappedBy="player",cascade=CascadeType.ALL,orphanRemoval = true)
	private List<Square> squares = new ArrayList<>();
	
	@Column
	private long lastBoatShooted;

	
	
	//GETTER & SETTER//
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Game> getGames() {
		return games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}

	public List<Boat> getBoats() {
		return boats;
	}

	public void setBoats(List<Boat> boats) {
		this.boats = boats;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isUser() {
		return isUser;
	}

	public void setUser(boolean isUser) {
		this.isUser = isUser;
	}

	public boolean getInit() {
		return isInit;
	}

	public void setInit(boolean isInit) {
		this.isInit = isInit;
	}

	public List<Square> getSquares() {
		return squares;
	}

	public void setSquares(List<Square> squares) {
		this.squares = squares;
	}

	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public long getLastBoatShooted() {
		return lastBoatShooted;
	}

	public void setLastBoatShooted(long lastBoatShooted) {
		this.lastBoatShooted = lastBoatShooted;
	}
}