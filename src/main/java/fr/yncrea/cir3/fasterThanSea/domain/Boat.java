package fr.yncrea.cir3.fasterThanSea.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="boat")
public class Boat {
	
	//COLUMN//
	@Id @Column
	@GeneratedValue(generator = "seqBoat")
	@SequenceGenerator(name = "seqBoat", sequenceName = "seq_boat", allocationSize=1)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private Character type;
	
	@Column
	private int life;
	
	@OneToMany(mappedBy="boat",cascade=CascadeType.ALL,orphanRemoval = true)
	private List<Square> squares = new ArrayList<>();
	
	@ManyToOne
	private Player player;
	
	@Column(nullable = false)
	private Boolean isPlaced;
	
	@OneToOne
	private Weapon weapon;



	//GETTER & SETTER//
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Character getType() {
		return type;
	}

	public void setType(Character type) {
		this.type = type;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public List<Square> getSquares() {
		return squares;
	}

	public void setSquares(List<Square> squares) {
		this.squares = squares;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public Boolean getIsPlaced() {
		return isPlaced;
	}

	public void setIsPlaced(Boolean isPlaced) {
		this.isPlaced = isPlaced;
	}

	public Weapon getWeapon() {
		return weapon;
	}

	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}
}