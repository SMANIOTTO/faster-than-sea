package fr.yncrea.cir3.fasterThanSea.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="store")
public class Store {
	
	//COLUMN//
	@Id @Column
	@GeneratedValue(generator = "seqStore")
	@SequenceGenerator(name = "seqStore", sequenceName = "seq_store", allocationSize=1)
	private Long id;
	
	@OneToMany(mappedBy="store",cascade=CascadeType.PERSIST,orphanRemoval = true)
	private List<Weapon> weapons;
	


	//GETTER & SETTER//
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Weapon> getWeapons() {
		return weapons;
	}

	public void setWeapons(List<Weapon> weapons) {
		this.weapons = weapons;
	}
}