package fr.yncrea.cir3.fasterThanSea.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="game")
public class Game {
		
	//COLUMN//
	@Id @Column
	@GeneratedValue(generator = "seqGame")
	@SequenceGenerator(name = "seqGame", sequenceName = "seq_game", allocationSize=1)
	private Long id;
	
	@Column(nullable=false)
	private boolean isWon;
	
	@ManyToMany
	private List<Player> players = new ArrayList<>();
	
	@Column
	private String name;
	
	@Column(nullable=false)
	private Boolean isFinish;

	

	//SETTER & GETTER//
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean getIsWon() {
		return isWon;
	}

	public void setWon(boolean isWon) {
		this.isWon = isWon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getIsFinish() {
		return isFinish;
	}

	public void setIsFinish(Boolean isFinish) {
		this.isFinish = isFinish;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}
}