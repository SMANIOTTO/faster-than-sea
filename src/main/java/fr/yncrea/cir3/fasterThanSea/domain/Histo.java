package fr.yncrea.cir3.fasterThanSea.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="histo")
public class Histo {
	//COLUMN//
	@Id @Column
	@GeneratedValue(generator = "seqHisto")
	@SequenceGenerator(name = "seqHisto", sequenceName = "sed_histo", allocationSize=1)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private String nameBoat;
	
	@Column
	private String touch;
	
	@Column(nullable=false)
	private int x;
	
	@Column(nullable=false)
	private int y;
	
	@ManyToOne
	private Game game;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameBoat() {
		return nameBoat;
	}

	public void setNameBoat(String nameBoat) {
		this.nameBoat = nameBoat;
	}
	
	public String getTouch() {
		return touch;
	}

	public void setTouch(String touch) {
		this.touch = touch;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
}