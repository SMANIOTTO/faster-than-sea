package fr.yncrea.cir3.fasterThanSea.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.yncrea.cir3.fasterThanSea.domain.Boat;
import fr.yncrea.cir3.fasterThanSea.domain.Game;
import fr.yncrea.cir3.fasterThanSea.domain.Histo;
import fr.yncrea.cir3.fasterThanSea.domain.Player;
import fr.yncrea.cir3.fasterThanSea.domain.Square;
import fr.yncrea.cir3.fasterThanSea.domain.Weapon;
import fr.yncrea.cir3.fasterThanSea.repository.BoatRepository;
import fr.yncrea.cir3.fasterThanSea.repository.GameRepository;
import fr.yncrea.cir3.fasterThanSea.repository.HistoRepository;
import fr.yncrea.cir3.fasterThanSea.repository.PlayerRepository;
import fr.yncrea.cir3.fasterThanSea.repository.SquareRepository;
import fr.yncrea.cir3.fasterThanSea.repository.WeaponRepository;

@Service
public class ResetService {
	
	int[] lifes = {5, 4, 3, 3, 2};
	Character[] types = {'a', 'c', 'd', 's', 't'};
	String[] names = {"Aircraft carrier", "Cruiser", "Destroyer", "Submarine", "Torpedo"};

	@Autowired
	private GameRepository gamesInDB;
	
	@Autowired
	private PlayerRepository playersInDB;
	
	@Autowired
	private SquareRepository squaresInDB;
	
	@Autowired
	private BoatRepository boatsInDB;
	
	@Autowired
	private WeaponRepository weaponsInDB;
	
	@Autowired
	private HistoRepository histoInDB;
	
	

	public void resetBoat(int numBoatToReset) {
		
		//1) Reset boat state
		Player player = playersInDB.findCurrentPlayer();
		Boat boatToReset = boatsInDB.findBoatsCrescentByPlayerId(player.getId()).get(numBoatToReset);
				
		//2) Reset Boat state
		boatToReset.setIsPlaced(false);
		
		//3) Reset boat's squares
		List<Square> squares = squaresInDB.findSquaresCrescentByPlayerId(player.getId());
		for(Square square : squares) {
			
			if(square.getBoat() != null && square.getBoat().getId() == boatToReset.getId()) {
				square.setState('e');
				square.setBoat(null);
			}			
		}
		
		//4) Reset boat weapons
		List<Weapon> weapons = weaponsInDB.findAll();
		for(Weapon weapon : weapons) {
			if(weapon.getBoat() != null && weapon.getBoat().getId() == boatToReset.getId()) {
				weapon.setBoat(null);
				weapon.setActive(false);
				weaponsInDB.save(weapon);
				break;
			}
		}
		
		boatToReset.setWeapon(null);
		
		//5) Save in DB
		squaresInDB.saveAll(squares);		
		boatsInDB.save(boatToReset);
	}
	
	
	
	public void resetGame() {
		
		//1) Delete ennemy boats from DB
		for(Boat boatToDelete : boatsInDB.findBoatsCrescentByPlayerId(playersInDB.findCurrentEnnemy().getId())) {
			boatsInDB.deleteById(boatToDelete.getId());
		}
				
		//2) Delete player boats from DB
		for(Boat boatToDelete : boatsInDB.findBoatsCrescentByPlayerId(playersInDB.findCurrentPlayer().getId())) {			
			if(boatToDelete.getWeapon() != null) {
				
				Weapon weaponToRemove = weaponsInDB.findByName(boatToDelete.getWeapon().getName());
				weaponToRemove.setBoat(null);
				weaponsInDB.save(weaponToRemove);
				
				boatToDelete.setWeapon(null);
			}
			boatsInDB.save(boatToDelete);
			boatsInDB.deleteById(boatToDelete.getId());
		}
		
		//3) Delete player squares from DB
		for(Square squareToDelete : squaresInDB.findSquaresCrescentByPlayerId(playersInDB.findCurrentPlayer().getId())) {
			squaresInDB.deleteById(squareToDelete.getId());
		}
		
		//4) Delete ennemy squares from DB
		for(Square squareToDelete : squaresInDB.findSquaresCrescentByPlayerId(playersInDB.findCurrentEnnemy().getId())) {
			squaresInDB.deleteById(squareToDelete.getId());
		}
		
		//5) Reset weapons
		for(Weapon weapon : weaponsInDB.findAll()) {
			weapon.setActive(false);
			weapon.setBuy(false);
			weapon.setStore(null);
			
			weaponsInDB.save(weapon);
		}
		
		//6) Delete histo from DB
		for(Histo histoToDelete : histoInDB.findCurrentHisto(gamesInDB.findCurrentGame().getId())) {
			histoInDB.deleteById(histoToDelete.getId());
		}
		
		//7) Set end game of game
		Game game = gamesInDB.findCurrentGame();
		List<Player> playerList = gamesInDB.findCurrentGame().getPlayers();
		playerList.remove(playersInDB.findCurrentEnnemy());
		game.setPlayers(playerList);
		game.setIsFinish(true);
		gamesInDB.save(game);
		
		//8) Delete ennemy from DB
		playersInDB.deleteById(playersInDB.findCurrentEnnemy().getId());
		gamesInDB.save(game);

		Player player = playersInDB.findCurrentPlayer();
		player.setCurrent(false);
		playersInDB.save(player);
		
		System.out.println("DB erase");
	}
}
