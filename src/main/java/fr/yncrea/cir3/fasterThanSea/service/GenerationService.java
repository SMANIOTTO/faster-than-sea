package fr.yncrea.cir3.fasterThanSea.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.yncrea.cir3.fasterThanSea.domain.Boat;
import fr.yncrea.cir3.fasterThanSea.domain.Game;
import fr.yncrea.cir3.fasterThanSea.domain.Player;
import fr.yncrea.cir3.fasterThanSea.domain.Square;
import fr.yncrea.cir3.fasterThanSea.domain.Store;
import fr.yncrea.cir3.fasterThanSea.domain.Weapon;
import fr.yncrea.cir3.fasterThanSea.form.CreationGameForm;
import fr.yncrea.cir3.fasterThanSea.repository.BoatRepository;
import fr.yncrea.cir3.fasterThanSea.repository.GameRepository;
import fr.yncrea.cir3.fasterThanSea.repository.PlayerRepository;
import fr.yncrea.cir3.fasterThanSea.repository.SquareRepository;
import fr.yncrea.cir3.fasterThanSea.repository.StoreRepository;
import fr.yncrea.cir3.fasterThanSea.repository.WeaponRepository;

@Service
public class GenerationService {
	
	int[] lifes = {5, 4, 3, 3, 2};
	Character[] types = {'a', 'c', 'd', 's', 't'};
	String[] names = {"Aircraft carrier", "Cruiser", "Destroyer", "Submarine", "Torpedo"};

	@Autowired
	private GameRepository gamesInDB;
	
	@Autowired
	private PlayerRepository playersInDB;
	
	@Autowired
	private SquareRepository squaresInDB;
	
	@Autowired
	private BoatRepository boatsInDB;
	
	@Autowired
	private WeaponRepository weaponsInDB;
	
	@Autowired
	private StoreRepository storeInDb;
	
	

	public void createGame(CreationGameForm creationGameForm) {
			
			//1) GAME
			Game game = new Game();
			
			game.setIsFinish(false);
			game.setName(creationGameForm.getGameName());
			
		
			//1.1) PLAYER
			Player player = new Player();
			
			player.setName(creationGameForm.getPlayerName());//NAME
			player.setUser(true);//USER
			player.setCurrent(true);//CURRENT
			player.setInit(true);//INIT
			player.setLastBoatShooted(-1);
			List<Game> gamesForPlayer = new ArrayList<Game>();//GAME
			gamesForPlayer.add(game);
				
				
			//1.1.1) SQUARE
			ArrayList<Square> playerSquares = new ArrayList<>();
			
			for(int y=0; y<10; y++) {
				for(int x=0; x<10; x++) {
					Square square = new Square();
					
					square.setState('e');//EMPTY state
					square.setX(x);//X
					square.setY(y);//Y
					square.setBoat(null);
					square.setPlayer(player);
					
					playerSquares.add(square);//Add square to the list
				}
			}
			player.setSquares(playerSquares);
			squaresInDB.saveAll(playerSquares);
				
					
			//1.1.2) BOAT
			ArrayList<Boat> playerBoats = new ArrayList<Boat>();	
			
			for(int i=0; i<5; i++) {//For each boat
				//2.1) Create boat
				Boat boat = new Boat();
				
				//2.2) Set valuer from form
				boat.setLife(lifes[i]);//LIFE
				if(creationGameForm.getBoatsName()[i] == "") {//EMPTY NAME
					boat.setName(names[i]);
				}
				else {//NAME FROM FORM
					boat.setName(creationGameForm.getBoatsName()[i]);
				}
				boat.setType(types[i]);//TYPE
				boat.setIsPlaced(false);//PLACE
				boat.setPlayer(player);//PLAYER
				
				//2.3) Add boat to the list
				playerBoats.add(boat);
			}
			player.setBoats(playerBoats);
			boatsInDB.saveAll(playerBoats);
					
					
			//1.2) Save player
			player.setGames(gamesForPlayer);
			List<Player> players = new ArrayList<Player>();
			players.add(player);
					
					
			//1.3) ENNEMY
			Player ennemy = new Player();
			
			List<Game> gamesForEnnemy = new ArrayList<Game>();
			gamesForEnnemy.add(game);
			ennemy.setGames(gamesForEnnemy);
			ennemy.setInit(false);
			ennemy.setUser(false);
			ennemy.setName("ennemy");
			ennemy.setCurrent(true);
				
			
			//1.3.1) SQUARE
			ArrayList<Square> ennemySquares = new ArrayList<>();
			
			for(int y=0; y<10; y++) {
				for(int x=0; x<10; x++) {
					Square square = new Square();
					
					square.setState('e');//EMPTY state
					square.setX(x);//X
					square.setY(y);//Y
					square.setBoat(null);
					square.setPlayer(ennemy);
					
					ennemySquares.add(square);//Add square to the list
				}
			}
			ennemy.setSquares(ennemySquares);
			gamesInDB.save(game);
			squaresInDB.saveAll(ennemySquares);
				
					
			//1.3.2) BOAT
			ArrayList<Boat> ennemyBoats = new ArrayList<Boat>();	
			
			for(int i=0; i<5; i++) {//For each boat
				//2.1) Create boat
				Boat boat = new Boat();
				
				//2.2) Set valuer from form
				boat.setLife(lifes[i]);//LIFE
				boat.setName(names[i]);
				boat.setType(types[i]);//TYPE
				boat.setIsPlaced(false);//PLACE
				boat.setPlayer(ennemy);//PLAYER
				
				//2.3) Add boat to the list
				ennemyBoats.add(boat);
			}
			ennemy.setBoats(ennemyBoats);
			boatsInDB.saveAll(ennemyBoats);
				
				
			//1.4) Save ennemy
			players.add(ennemy);
				
				
			//2) Save game
			game.setPlayers(players);
				
			
			//3) Save in DB
			playersInDB.saveAll(players);
			gamesInDB.save(game);
	}
	
	
	
	public void createWeapons() {
		
		//1) Check weapon create in DB
		if(weaponsInDB.findAll().isEmpty() && weaponsInDB.findAll().size() < 3) {
			
			//1.1) Create store
			Store store = new Store();
			storeInDb.save(store);
			
			//1.2) Create weapon
			List<Weapon> weapons = new ArrayList<Weapon>();
			int[] damage = {2, 4, 3};
			String[] effect = {"line", "area", "random"};
			String[] name = {"torpedo", "bomb", "mortar"};
			int[] size = {2, 5, 3};
			String[] description = {"Fire on line of 2 cases", "Fire on area of 2x2 cases", "Fire on random 3 cases"};
			
			for(int i=0; i<3; i++) {
				Weapon weapon = new Weapon();
				
				weapon.setDamage(damage[i]);
				weapon.setEffect(effect[i]);
				weapon.setName(name[i]);
				weapon.setSize(size[i]);
				weapon.setDescription(description[i]);
				weapon.setBuy(false);
				weapon.setActive(false);
				
				weapon.setStore(store);
				weapons.add(weapon);
				weaponsInDB.save(weapon);
			}
			
			//1.2.4) Add weapon to the store
			store.setWeapons(weapons);
			storeInDb.save(store);
			
			System.out.println("Weapons store created");
		}
	}
	
	
	
	public void createEnnemy(Player ennemy, List<Square> ennemySquares, List<Boat> ennemyBoats) {
		
		//1) Set ennemy initialised state
		ennemy.setInit(true);
		playersInDB.save(ennemy);
		
		//2) Create and init boats
		for(Boat boat : ennemyBoats) {
			boat.setIsPlaced(true);//PLACE				
		}
		boatsInDB.saveAll(ennemyBoats);
		
		//3) Position ennemy
		List<Boat> ennemyBoatsToSend = boatsInDB.findBoatsCrescentByPlayerId(ennemy.getId());
		for(Boat ennemyBoat : ennemyBoatsToSend) {
		
			//2.3.1) Set orientation
			Random rand = new Random();
			Boolean isVertical = rand.nextBoolean();
			
			//2.3.2) Test for boat entering in grid
			int x=0;
			int y=0;
			Boolean isPossibleToPlace = true;
			List<Square> squaresForEnnemyBoat = new ArrayList<Square>();
			do {
				isPossibleToPlace = true;
				squaresForEnnemyBoat.clear();
				
				//2.3.2.1) Get random coords
				x = rand.nextInt(10 - ennemyBoat.getLife());
				y = rand.nextInt(10);
				if(isVertical) {
					y = y < ennemyBoat.getLife()-1 ? y+ennemyBoat.getLife()-1 : y; 
				}
				
				//2.3.2.2) Test if boat enter in grid
				if( (isVertical && y - (ennemyBoat.getLife()-1) < 0) || (!isVertical && x + (ennemyBoat.getLife()-1) > 9) ) {
					isPossibleToPlace = false;
					System.out.println("Not entering in grid : " + ennemyBoat.getName() + ':' + x + ':' + y + ':' + isVertical);
				}
				
				//2.3.2.3) Test if no boat on path
				for(int j=0; j<ennemyBoat.getLife(); j++) {
					
					if(isPossibleToPlace) {
						
						for(Square ennemySquare : ennemySquares) {

							if(isVertical && ennemySquare.getX() == x && ennemySquare.getY() == y-j) {//VERTICAL
								if(ennemySquare.getState() == 'e') {
									squaresForEnnemyBoat.add(ennemySquare);
									//System.out.println("c " + ennemySquare.getX() + ":" + ennemySquare.getY() + " OK");
									break;
								}
								else {
									//System.out.println("c " + ennemySquare.getX() + ":" + ennemySquare.getY() + "NOT OK");
									isPossibleToPlace = false;
									break;
								}
							}
							else if(!isVertical && ennemySquare.getX() == x+j && ennemySquare.getY() == y) {//HORIZONTAL
								if(ennemySquare.getState() == 'e') {
									squaresForEnnemyBoat.add(ennemySquare);
									//System.out.println("c " + ennemySquare.getX() + ":" + ennemySquare.getY() + " OK");
									break;
								}
								else {
									//System.out.println("c " + ennemySquare.getX() + ":" + ennemySquare.getY() + "NOT OK");
									isPossibleToPlace = false;
									break;
								}
							}
						}
					}
				}
				
			}
			while(!isPossibleToPlace);//No boat on path & inside grid
			
			
			//2.3.2.4) Place it
			if(isPossibleToPlace) {
				ennemyBoat.setIsPlaced(true);
				
				//2.3.2.4.1) Set squares states
				ArrayList<Square> squaresToAdd = new ArrayList<Square>();
				for(Square squareForEnnemyBoat : squaresForEnnemyBoat) {
					Square squareToAdd = squaresInDB.findSquareById(squareForEnnemyBoat.getId());//Get square from DB
					
					squareToAdd.setState('f');
					squareToAdd.setBoat(ennemyBoat);
					squaresToAdd.add(squareToAdd);
					squaresInDB.save(squareToAdd);
				}
				System.out.println(ennemyBoat.getName() + " square saved");
				
				//2.3.2.4.2) Give square to boat
				boatsInDB.save(ennemyBoat);
				
				squaresForEnnemyBoat.clear();
			}
		}
	}

}
