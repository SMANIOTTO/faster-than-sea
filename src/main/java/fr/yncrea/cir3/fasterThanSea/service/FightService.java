package fr.yncrea.cir3.fasterThanSea.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.yncrea.cir3.fasterThanSea.domain.Boat;
import fr.yncrea.cir3.fasterThanSea.domain.Histo;
import fr.yncrea.cir3.fasterThanSea.domain.Player;
import fr.yncrea.cir3.fasterThanSea.domain.Square;
import fr.yncrea.cir3.fasterThanSea.domain.Weapon;
import fr.yncrea.cir3.fasterThanSea.repository.BoatRepository;
import fr.yncrea.cir3.fasterThanSea.repository.GameRepository;
import fr.yncrea.cir3.fasterThanSea.repository.HistoRepository;
import fr.yncrea.cir3.fasterThanSea.repository.PlayerRepository;
import fr.yncrea.cir3.fasterThanSea.repository.SquareRepository;

@Service
public class FightService {
	
	int[] lifes = {5, 4, 3, 3, 2};
	Character[] types = {'a', 'c', 'd', 's', 't'};
	String[] names = {"Aircraft carrier", "Cruiser", "Destroyer", "Submarine", "Torpedo"};

	@Autowired
	private GameRepository gamesInDB;
	
	@Autowired
	private PlayerRepository playersInDB;
	
	@Autowired
	private SquareRepository squaresInDB;
	
	@Autowired
	private BoatRepository boatsInDB;
	
	@Autowired
	private HistoRepository histoInDB;
	
	

	public void playerShoot(long idLastBoatShooted, int x, int y) {
		
		//1) Get current data
		long ennemyId = playersInDB.findCurrentEnnemy().getId();
		String playerName = playersInDB.findCurrentPlayer().getName();
		Boat lastBoatShooted = boatsInDB.findBoatById(idLastBoatShooted);
		Weapon weapon = lastBoatShooted.getWeapon();
		ArrayList<Histo> histoList = histoInDB.findCurrentHisto(gamesInDB.findCurrentGame().getId());
		Histo histoPlayer = new Histo();
		
		//1.2) Set damage on type
		if(lastBoatShooted.getWeapon() != null) {
			System.out.println("weapon effect : " + weapon.getEffect());
			if(weapon.getEffect().equals("line")) {//TORPEDO
				System.out.println("line weapon");
				for(int i=0; i<2; i++) {
					if(x+i <= 9) {
						Square squareTouch = squaresInDB.findSquareByCoordAndPlayerId(x+i, y, ennemyId);
						
						if(squareTouch.getState() == 'e') {
							squareTouch.setState('n');//NOTHING TOUCH

							histoPlayer.setGame(gamesInDB.findCurrentGame());
							histoPlayer.setName(playerName);
							histoPlayer.setTouch("Manque");
							histoPlayer.setNameBoat(lastBoatShooted.getName());
							histoPlayer.setX(squareTouch.getX());
							histoPlayer.setY(squareTouch.getY());
							System.out.println(histoPlayer.getTouch() + " " + histoPlayer.getName() + " avec " + histoPlayer.getNameBoat() + " en " + histoPlayer.getX() + " : " + histoPlayer.getY());
							histoList.add(histoPlayer);
							histoInDB.saveAll(histoList);
						}
						else if(squareTouch.getState() == 'f') {
							
							//1.1) Set touch state
							squareTouch.setState('d');//DESTROY
							
							//1.2) Update boat life
							Boat boatTouch = boatsInDB.findBoatById(squareTouch.getBoat().getId());
							boatTouch.setLife(boatTouch.getLife()-1);
							boatsInDB.save(boatTouch);

							histoPlayer.setGame(gamesInDB.findCurrentGame());
							histoPlayer.setName(playerName);
							histoPlayer.setTouch("Touche");
							histoPlayer.setNameBoat(lastBoatShooted.getName());
							histoPlayer.setX(squareTouch.getX());
							histoPlayer.setY(squareTouch.getY());
							System.out.println(histoPlayer.getTouch() + " " + histoPlayer.getName() + " avec " + histoPlayer.getNameBoat() + " en " + histoPlayer.getX() + " : " + histoPlayer.getY());
							histoList.add(histoPlayer);
							histoInDB.saveAll(histoList);					
						}
						else {
							System.out.println("ERROR : state case touch");
						}
						squaresInDB.save(squareTouch);
					}
				}
			}
			
			
			else if(weapon.getEffect().equals("area")) {//BOMB
				System.out.println("area weapon");
				for(int i=0; i<2; i++) {
					for(int j=0; j<2; j++) {
						if(x+i <= 9 && y+j <= 9) {
							Square squareTouch = squaresInDB.findSquareByCoordAndPlayerId(x+i, y+j, ennemyId);
							
							if(squareTouch.getState() == 'e') {
								squareTouch.setState('n');//NOTHING TOUCH

								histoPlayer.setGame(gamesInDB.findCurrentGame());
								histoPlayer.setName(playerName);
								histoPlayer.setTouch("Manque");
								histoPlayer.setNameBoat(lastBoatShooted.getName());
								histoPlayer.setX(squareTouch.getX());
								histoPlayer.setY(squareTouch.getY());
								System.out.println(histoPlayer.getTouch() + " " + histoPlayer.getName() + " avec " + histoPlayer.getNameBoat() + " en " + histoPlayer.getX() + " : " + histoPlayer.getY());
								histoList.add(histoPlayer);
								histoInDB.saveAll(histoList);
								//System.out.println("missed ennemy " + squareTouch.getX() + " : " + squareTouch.getY());
							}
							else if(squareTouch.getState() == 'f') {
								
								//1.1) Set touch state
								squareTouch.setState('d');//DESTROY
								
								//1.2) Update boat life
								Boat boatTouch = boatsInDB.findBoatById(squareTouch.getBoat().getId());
								boatTouch.setLife(boatTouch.getLife()-1);
								boatsInDB.save(boatTouch);

								histoPlayer.setGame(gamesInDB.findCurrentGame());
								histoPlayer.setName(playerName);
								histoPlayer.setTouch("Touche");
								histoPlayer.setNameBoat(lastBoatShooted.getName());
								histoPlayer.setX(squareTouch.getX());
								histoPlayer.setY(squareTouch.getY());
								System.out.println(histoPlayer.getTouch() + " " + histoPlayer.getName() + " avec " + histoPlayer.getNameBoat() + " en " + histoPlayer.getX() + " : " + histoPlayer.getY());
								histoList.add(histoPlayer);
								histoInDB.saveAll(histoList);
								
								//System.out.println("Touch ennemy " + squareTouch.getX() + " : " + squareTouch.getY());
							}
							else {
								System.out.println("ERROR : state case touch");
							}
							squaresInDB.save(squareTouch);
						}
					}
				}
			}
			
			
			else if(weapon.getEffect().equals("random")) {//MORTAR
				System.out.println("random weapon");
				Random rand = new Random();
				for(int i=0; i<3; i++) {
					List<Square> squaresRemaining = squaresInDB.findSquareRemaining(ennemyId);
					
					//1) Get random index in remaining square
					int index = rand.nextInt(squaresRemaining.size()-1);
					
					//2) Set square touch
					Square squareTouch = squaresRemaining.get(index);
					
					if(squareTouch.getState() == 'e') {
						squareTouch.setState('n');//NOTHING TOUCH

						histoPlayer.setGame(gamesInDB.findCurrentGame());
						histoPlayer.setName(playerName);
						histoPlayer.setTouch("Manque");
						histoPlayer.setNameBoat(lastBoatShooted.getName());
						histoPlayer.setX(squareTouch.getX());
						histoPlayer.setY(squareTouch.getY());
						System.out.println(histoPlayer.getTouch() + " " + histoPlayer.getName() + " avec " + histoPlayer.getNameBoat() + " en " + histoPlayer.getX() + " : " + histoPlayer.getY());
						histoList.add(histoPlayer);
						histoInDB.saveAll(histoList);
						//System.out.println("missed ennemy " + squareTouch.getX() + " : " + squareTouch.getY());
					}
					else if(squareTouch.getState() == 'f') {
						
						//1.1) Set touch state
						squareTouch.setState('d');//DESTROY
						
						//1.2) Update boat life
						Boat boatTouch = boatsInDB.findBoatById(squareTouch.getBoat().getId());
						boatTouch.setLife(boatTouch.getLife()-1);
						boatsInDB.save(boatTouch);


						histoPlayer.setGame(gamesInDB.findCurrentGame());
						histoPlayer.setName(playerName);
						histoPlayer.setTouch("Touche");
						histoPlayer.setNameBoat(lastBoatShooted.getName());
						histoPlayer.setX(squareTouch.getX());
						histoPlayer.setY(squareTouch.getY());
						System.out.println(histoPlayer.getTouch() + " " + histoPlayer.getName() + " avec " + histoPlayer.getNameBoat() + " en " + histoPlayer.getX() + " : " + histoPlayer.getY());
						histoList.add(histoPlayer);
						histoInDB.saveAll(histoList);
						//System.out.println("Touch ennemy " + squareTouch.getX() + " : " + squareTouch.getY());
					}
					else {
						System.out.println("ERROR : state case touch");
					}
					squaresInDB.save(squareTouch);
				}
			}
		}
		
		//NO BOMB = NORMAL SHOOT
		else {
			Square squareTouch = squaresInDB.findSquareByCoordAndPlayerId(x, y, ennemyId);
			
			if(squareTouch.getState() == 'e') {
				squareTouch.setState('n');//NOTHING TOUCH

				histoPlayer.setGame(gamesInDB.findCurrentGame());
				histoPlayer.setName(playerName);
				histoPlayer.setTouch("Manque");
				histoPlayer.setNameBoat(lastBoatShooted.getName());
				histoPlayer.setX(squareTouch.getX());
				histoPlayer.setY(squareTouch.getY());
				System.out.println(histoPlayer.getTouch() + " " + histoPlayer.getName() + " avec " + histoPlayer.getNameBoat() + " en " + histoPlayer.getX() + " : " + histoPlayer.getY());
				histoList.add(histoPlayer);
				histoInDB.saveAll(histoList);
			}
			else if(squareTouch.getState() == 'f') {
				
				//1.1) Set touch state
				squareTouch.setState('d');//DESTROY
				
				//1.2) Update boat life
				Boat boatTouch = boatsInDB.findBoatById(squareTouch.getBoat().getId());
				boatTouch.setLife(boatTouch.getLife()-1);
				boatsInDB.save(boatTouch);

				histoPlayer.setGame(gamesInDB.findCurrentGame());
				histoPlayer.setName(playerName);
				histoPlayer.setTouch("Touche");
				histoPlayer.setNameBoat(lastBoatShooted.getName());
				histoPlayer.setX(squareTouch.getX());
				histoPlayer.setY(squareTouch.getY());
				System.out.println(histoPlayer.getTouch() + " " + histoPlayer.getName() + " avec " + histoPlayer.getNameBoat() + " en " + histoPlayer.getX() + " : " + histoPlayer.getY());
				histoList.add(histoPlayer);
				histoInDB.saveAll(histoList);
				
				//System.out.println("Touch ennemy " + squareTouch.getX() + " : " + squareTouch.getY());
			}
			else {
				System.out.println("ERROR : state case touch");
			}
			squaresInDB.save(squareTouch);
		}
		
		
		//2) Set new boat shooted
		Player player = playersInDB.findCurrentPlayer();
		List<Boat> playerBoats = boatsInDB.findBoatsCrescentByPlayerId(player.getId());
		boolean isFind = false;
		for(int i=0; i<playerBoats.size()-1; i++) {
			if(playerBoats.get(i).getId() == idLastBoatShooted) {
				player.setLastBoatShooted(playerBoats.get(i+1).getId());
				isFind = true;
				break;
			}
		}
		if(!isFind) {
			player.setLastBoatShooted(playerBoats.get(0).getId());
		}
		playersInDB.save(player);
	}
	
	
	
	public void ennemyShoot() {
		
		//3) Ennemy choose square to shoot
		Histo histoEnnemy = new Histo();
		List<Square> playerSquaresRemaining = squaresInDB.findSquareRemaining(playersInDB.findCurrentPlayer().getId());
		String ennemyName = playersInDB.findCurrentEnnemy().getName();
		Random rand = new Random();
		int index = rand.nextInt(playerSquaresRemaining.size());
		ArrayList<Histo> histoList = histoInDB.findCurrentHisto(gamesInDB.findCurrentGame().getId());
		
		//4) Check if touch
		if(playerSquaresRemaining.get(index).getState() == 'e') {//EMPTY
			playerSquaresRemaining.get(index).setState('n');

			histoEnnemy.setGame(gamesInDB.findCurrentGame());
			histoEnnemy.setName(ennemyName);
			histoEnnemy.setTouch("Manque");
			histoEnnemy.setNameBoat("");
			histoEnnemy.setX(playerSquaresRemaining.get(index).getX());
			histoEnnemy.setY(playerSquaresRemaining.get(index).getY());
			System.out.println(histoEnnemy.getTouch() + " " + histoEnnemy.getName() + " avec " + histoEnnemy.getNameBoat() + " en " + histoEnnemy.getX() + " : " + histoEnnemy.getY());
			histoList.add(histoEnnemy);
			histoInDB.saveAll(histoList);
			//System.out.println("missed player " + playerSquaresRemaining.get(index).getX() + " : " + playerSquaresRemaining.get(index).getY());
		}
		else if(playerSquaresRemaining.get(index).getState() == 'f') {//FULL
			
			//4.1) Set state
			playerSquaresRemaining.get(index).setState('d');
			
			//4.2) Update boat life
			long boatId = playerSquaresRemaining.get(index).getBoat().getId();
			Boat boatTouch = boatsInDB.findBoatById(boatId);
			boatTouch.setLife(boatTouch.getLife()-1);
			
			//1.3) Save boat in DB
			boatsInDB.save(boatTouch);

			histoEnnemy.setGame(gamesInDB.findCurrentGame());
			histoEnnemy.setName(ennemyName);
			histoEnnemy.setTouch("Touche");
			histoEnnemy.setNameBoat(boatTouch.getName());
			histoEnnemy.setX(playerSquaresRemaining.get(index).getX());
			histoEnnemy.setY(playerSquaresRemaining.get(index).getY());
			System.out.println(histoEnnemy.getTouch() + " " + histoEnnemy.getName() + " avec " + histoEnnemy.getNameBoat() + " en " + histoEnnemy.getX() + " : " + histoEnnemy.getY());
			histoList.add(histoEnnemy);
			histoInDB.saveAll(histoList);
			
			//System.out.println("Touch player " + playerSquaresRemaining.get(index).getX() + " : " + playerSquaresRemaining.get(index).getY());
		}
		else {
			System.out.println("ERROR : CHOOSE RANDOM PLAYER SQUARE CORD");
		}
		
		//5) Save square in DB
		squaresInDB.save(playerSquaresRemaining.get(index));
	}

}
