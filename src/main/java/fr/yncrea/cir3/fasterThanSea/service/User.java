package fr.yncrea.cir3.fasterThanSea.service;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import fr.yncrea.cir3.fasterThanSea.domain.Authority;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "\"user\"")
public class User implements UserDetails {
	private static final long serialVersionUID = 1L;

	@Id @Column
	@GeneratedValue(generator = "seqAccount")
	@SequenceGenerator(name = "seqAccount", sequenceName = "seq_account")
	private long id;

	@Column(length = 100)
	private String username;
	
	@Column(length = 100)
	private String password;
	

	@ManyToMany(fetch=FetchType.EAGER)
	private Collection<Authority> authorities;

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return this.password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return this.username;
	}

	public void setUsername(String username2) {
		this.username = new String(username2);
		
	}

	public void setPassword(String password2) {
		// TODO Auto-generated method stub
		this.password = new String(password2);
	}
	
	public void setId(long i) {
		this.id = i;
	}
	public long getId() {
		return this.id;
	}
}
