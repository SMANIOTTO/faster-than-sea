package fr.yncrea.cir3.fasterThanSea.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.yncrea.cir3.fasterThanSea.domain.Boat;
import fr.yncrea.cir3.fasterThanSea.domain.Square;
import fr.yncrea.cir3.fasterThanSea.domain.Weapon;
import fr.yncrea.cir3.fasterThanSea.repository.BoatRepository;
import fr.yncrea.cir3.fasterThanSea.repository.SquareRepository;
import fr.yncrea.cir3.fasterThanSea.repository.WeaponRepository;

@Service
public class PlacementService {
	
	int[] lifes = {5, 4, 3, 3, 2};
	Character[] types = {'a', 'c', 'd', 's', 't'};
	String[] names = {"Aircraft carrier", "Cruiser", "Destroyer", "Submarine", "Torpedo"};
	
	@Autowired
	private SquareRepository squaresInDB;
	
	@Autowired
	private BoatRepository boatsInDB;
	
	@Autowired
	private WeaponRepository weaponsInDB;
	
	

	public void placePlayerBoat(int numShipSelected, boolean isVertical, int x, int y, long playerId) {
		
		//1) Test if boat can be place
		Boat boat = boatsInDB.findBoatsCrescentByPlayerId(playerId).get(numShipSelected);
		
		System.out.println("Ship chose : " + boat.getName());
		
		if(!boat.getIsPlaced()) {
			if((isVertical && y - (boat.getLife()-1) >= 0) || (!isVertical && x + (boat.getLife()-1) <= 9)) {//POSSIBLE
				
				//1.1) Test if no boat on path
				ArrayList<Square> squaresToPlace = new ArrayList<Square>();
				Boolean isPossibleToPlace = true;
				for(int i=0; i<boat.getLife(); i++) {
					
					//1.1.1) Get the square from DB
					Square squareToPlace = new Square();
					if(isVertical) {
						squareToPlace = squaresInDB.findSquareByCoordAndPlayerId(x, y-i, playerId);
					}
					else {
						squareToPlace = squaresInDB.findSquareByCoordAndPlayerId(x+i, y, playerId);
					}
					
					//1.1.2) Test if other ship on path
					if(squareToPlace.getState() == 'e') {//NO SHIP					
						squaresToPlace.add(squareToPlace);
					}
					else {//SHIP
						squaresToPlace.clear();
						isPossibleToPlace = false;
						break;
					}
				}
				
				//1.2) Set state and boat
				if(isPossibleToPlace) {
					
					//1.2.1) Add new state to ship list
					for (Square squareToChangeState : squaresToPlace) {
						squareToChangeState.setState('f');
						squareToChangeState.setBoat(boat);
					}
					
					//1.2.2) Set boat in placed state
					boat.setIsPlaced(true);
					
					//1.2.2) Save in DB
					boatsInDB.save(boat);
					squaresInDB.saveAll(squaresToPlace);
				}
				
				//IMPOSSIBLE SHIP ON PATH
				else {
					System.out.println("IMPOSSIBLE TO PLACE : SHIP ON PATH");
				}
			}
		
			//IMPOSSIBLE SIZE MIN TOO SHORT
			else{
				System.out.println("IMPOSSIBLE TO PLACE : SIZE MIN TOO SHORT");
			}
		}
		else {
			System.out.println("IMPOSSIBLE TO PLACE : SHIP ALREADY PLACE");
		}
	}
	
	
	
	public void setWeaponOnBoat(Weapon weapon, Boat boat) {
		
		//1) Check size available && no weapon
		if(weapon.getSize() <= boat.getLife() && boat.getWeapon() == null) {
			
			//3.1) Set weapon date
			weapon.setBoat(boat);
			weapon.setActive(true);
			weaponsInDB.save(weapon);
			
			boat.setWeapon(weapon);
			boatsInDB.save(boat);
			
			System.out.println("Weapon " + weapon.getName() + " added to boat " + boat.getName());
		}
		else {
			System.out.println("ERROR : size boat " + boat.getLife() + " > " + weapon.getSize());
		}
	}
	
}
