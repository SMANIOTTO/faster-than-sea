package fr.yncrea.cir3.fasterThanSea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FasterThanSeaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FasterThanSeaApplication.class, args);
	}

}
