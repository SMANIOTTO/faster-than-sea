package fr.yncrea.cir3.fasterThanSea.form;

public class CreationGameForm {

	//GAME VAR//
	private Long id;
	
	private String gameName;
	
	private String playerName;
	
	private String[] boatsName = new String[5];

	
	//GETTER & SETTER//
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String[] getBoatsName() {
		return boatsName;
	}

	public void setBoatsName(String[] boatsName) {
		this.boatsName = boatsName;
	}
}
