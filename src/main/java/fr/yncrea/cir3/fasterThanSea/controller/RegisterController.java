package fr.yncrea.cir3.fasterThanSea.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import fr.yncrea.cir3.fasterThanSea.dto.UserForm;
import fr.yncrea.cir3.fasterThanSea.repository.UserRepository;
import fr.yncrea.cir3.fasterThanSea.service.User;

@Controller
public class RegisterController {
	
	@Autowired
	private UserRepository userInDB;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@GetMapping("/register")
	public String showRegistrationForm(WebRequest request, Model model) {
	    UserForm userForm = new UserForm();
	    model.addAttribute("userForm", userForm);
	    return "register";
	}
	
	@PostMapping("/register")
	public String saveUser(@Valid @ModelAttribute("userForm") UserForm userForm, BindingResult result) {
		 
		User user = new User();
		user.setId(userInDB.count()+1);
		user.setPassword(passwordEncoder.encode(userForm.getPassword()));
		user.setUsername(userForm.getUsername());
		System.out.println(user.getUsername());
	    System.out.println(user.getPassword());
		userInDB.save(user);
		
		return "login";
	}

}
