package fr.yncrea.cir3.fasterThanSea.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import fr.yncrea.cir3.fasterThanSea.domain.Boat;
import fr.yncrea.cir3.fasterThanSea.domain.Game;
import fr.yncrea.cir3.fasterThanSea.domain.Player;
import fr.yncrea.cir3.fasterThanSea.domain.Square;
import fr.yncrea.cir3.fasterThanSea.domain.Weapon;
import fr.yncrea.cir3.fasterThanSea.form.CreationGameForm;
import fr.yncrea.cir3.fasterThanSea.repository.BoatRepository;
import fr.yncrea.cir3.fasterThanSea.repository.GameRepository;
import fr.yncrea.cir3.fasterThanSea.repository.PlayerRepository;
import fr.yncrea.cir3.fasterThanSea.repository.SquareRepository;
import fr.yncrea.cir3.fasterThanSea.repository.WeaponRepository;
import fr.yncrea.cir3.fasterThanSea.service.FightService;
import fr.yncrea.cir3.fasterThanSea.service.GenerationService;
import fr.yncrea.cir3.fasterThanSea.service.PlacementService;
import fr.yncrea.cir3.fasterThanSea.service.ResetService;
import fr.yncrea.cir3.fasterThanSea.repository.HistoRepository;



@Controller
@RequestMapping("/game")
public class gameController {

	//REPOSITORY//
	@Autowired
	private GameRepository gamesInDB;
	
	@Autowired
	private PlayerRepository playersInDB;
	
	@Autowired
	private SquareRepository squaresInDB;
	
	@Autowired
	private BoatRepository boatsInDB;
	
	@Autowired
	private WeaponRepository weaponsInDB;
	
	@Autowired
	private HistoRepository histoInDB;
	
	
	
	//SERVICE//
	@Autowired
	private GenerationService generationService;
	
	@Autowired
	private PlacementService placementService;
	
	@Autowired
	private ResetService resetService;
	
	@Autowired
	private FightService fightService;
	
	
	
	//CONFIGURATION//
	@GetMapping("/configuration")
	public String configure(Model model) {
		
		//1) Create game form to send
		CreationGameForm creationGameForm = new CreationGameForm();
		
		//2) Send the form
		model.addAttribute("creationGameForm", creationGameForm);
		
		return "gameConfig";
	}
	
	
	
	//GENERATION//
	@PostMapping("/generation")
	public String generation(@ModelAttribute CreationGameForm creationGameForm) {
		
		//1) Create game
		generationService.createGame(creationGameForm);
		
		//2) Create weapons if necessary
		generationService.createWeapons();
				
		return "redirect:/game/preparation/0/true";
	}
	
	
	
	//SHIP SELECTION//
	@GetMapping("/preparation/{numShipSelected}/{isVertical}")
	public String shipSelection(@PathVariable int numShipSelected, @PathVariable Boolean isVertical, Model model) {
		long playerId = playersInDB.findCurrentPlayer().getId();
		
		//1) Send square list
		model.addAttribute("squares", squaresInDB.findSquaresCrescentByPlayerId(playerId));
		
		//2) Send boat list
		model.addAttribute("boats", boatsInDB.findBoatsCrescentByPlayerId(playerId));//BOAT
		
		//3) Send weapon list
		model.addAttribute("weapons", weaponsInDB.findWeaponCrescent());//BOAT

		return "gamePreparation";
	}
	
	
	
	//SHIP PLACEMENT//
	@GetMapping("/preparation/{numShipSelected}/{isVertical}/{x}/{y}")
	public String shipSelection(@PathVariable int numShipSelected, @PathVariable Boolean isVertical, 
			@PathVariable int x, @PathVariable int y, Model model) {
		long playerId = playersInDB.findCurrentPlayer().getId();
		
		//1) place boat if possible
		placementService.placePlayerBoat(numShipSelected, isVertical, x, y, playerId);
	
		//2) Send square list
		model.addAttribute("squares", squaresInDB.findSquaresCrescentByPlayerId(playerId));
		
		//3) Send boat list
		model.addAttribute("boats", boatsInDB.findBoatsCrescentByPlayerId(playerId));
		
		//4) Send weapon list
		model.addAttribute("weapons", weaponsInDB.findWeaponCrescent());//BOAT
		
		return "gamePreparation";
	}
	
	
	
	//SHIP RESET//
	@GetMapping("/preparation/reset/{numBoatToReset}/{isVertical}")
	public String resetShip(@PathVariable int numBoatToReset, @PathVariable Boolean isVertical) {
				
		//1) Reset boat and associated settings (square & weapon)
		resetService.resetBoat(numBoatToReset);
		
		return "redirect:/game/preparation/" + numBoatToReset + "/" + isVertical;
	}
	
	
	
	//SHIP WEAPON//
	@GetMapping("/preparation/set/{nameWeapon}/{idBoat}/{numShipSelected}/{isVertical}")
	public String setWeapon(@PathVariable String nameWeapon, @PathVariable long idBoat, @PathVariable int numShipSelected, @PathVariable boolean isVertical) {
				
		//1) Get boat from DB
		Boat boat = boatsInDB.findById(idBoat).get();
		
		//2) Get weapon from DB
		Weapon weapon = weaponsInDB.findByName(nameWeapon);
		
		//3) Set weapon on boat
		placementService.setWeaponOnBoat(weapon, boat);
		
		return "redirect:/game/preparation/" + numShipSelected + '/' + isVertical;
	}
	
	
	
	//FIGHT//
	@GetMapping("/fight")
	public String fight(Model model) {
		
		//1) Get current data
		Game game = gamesInDB.findCurrentGame();
		Player ennemy = playersInDB.findCurrentEnnemy();
		List<Square> ennemySquares = squaresInDB.findSquaresCrescentByPlayerId(playersInDB.findCurrentEnnemy().getId());
		List<Boat> ennemyBoats = boatsInDB.findBoatsCrescentByPlayerId(playersInDB.findCurrentEnnemy().getId());
		
		//2) Init ennemy if necessary
		if(!ennemy.getInit()) {
			generationService.createEnnemy(ennemy, ennemySquares, ennemyBoats);
		}
		
		//3) Get players life remaining
		long playerId = playersInDB.findCurrentPlayer().getId();
		int nbPlayerLifeRemaining = 0;
		for(Boat playerBoat : boatsInDB.findBoatsCrescentByPlayerId(playerId)) {
			nbPlayerLifeRemaining += playerBoat.getLife();
		}
		
		//4) Check ennemy nb life remaining
		long ennemyId = playersInDB.findCurrentEnnemy().getId();
		int nbEnnemyLifeRemaining = 0;
		for(Boat ennemyBoat : boatsInDB.findBoatsCrescentByPlayerId(ennemyId)) {
			nbEnnemyLifeRemaining += ennemyBoat.getLife();
		}
		
		//5) Set default starting boat
		if(playersInDB.findCurrentPlayer().getLastBoatShooted() == -1) {
			Player player = playersInDB.findCurrentPlayer();
			player.setLastBoatShooted(boatsInDB.findBoatsCrescentByPlayerId(playerId).get(0).getId());
			playersInDB.save(player);
		}
		
		//6) Check victory OR defeat
		if(nbEnnemyLifeRemaining == 0) {//LOOSE
			game.setWon(true);
		}
		else if(nbPlayerLifeRemaining == 0) {//WIN
			game.setWon(false);
		}
		
		//7) Save
		gamesInDB.save(game);
		
		//8) Get game data from DB				
		model.addAttribute("ennemySquares", squaresInDB.findSquaresCrescentByPlayerId(ennemyId));
		model.addAttribute("playerSquares", squaresInDB.findSquaresCrescentByPlayerId(playerId));
		model.addAttribute("playerLifeRemaining", nbPlayerLifeRemaining);
		model.addAttribute("ennemyLifeRemaining", nbEnnemyLifeRemaining);
		model.addAttribute("isWon", game.getIsWon());
		model.addAttribute("boatShooting", boatsInDB.findBoatById(playersInDB.findCurrentPlayer().getLastBoatShooted()));
		model.addAttribute("histoList", histoInDB.findCurrentHisto(gamesInDB.findCurrentGame().getId()));
		
		return "gameFight";
	}
	
	
	
	//SHOOT//
	@GetMapping("/fight/{x}/{y}/{idLastBoatShooted}")
	public String fight(Model model, @PathVariable int x, @PathVariable int y, @PathVariable long idLastBoatShooted) {
		
		//1) Player shoot
		fightService.playerShoot(idLastBoatShooted, x, y);
		
		//2) Ennemy shoot
		fightService.ennemyShoot();
		
		return "redirect:/game/fight";
	}
	
	
	
	//END//
	@GetMapping("/end")
	public String end() {		
		
		//1) Reset all the game (not player and game)
		resetService.resetGame();
		
		return "redirect:/";
	}
	
	
	
	//STORE//
	@GetMapping("/store/{numShipSelected}/{isVertical}")
	public String store(Model model, @PathVariable int numShipSelected, @PathVariable boolean isVertical) {	
		
		//1) Send weapon and settings
		model.addAttribute("weapons", weaponsInDB.findWeaponCrescent());
		model.addAttribute("numShipSelected", numShipSelected);
		model.addAttribute("isVertical", isVertical);
		
		return "store";
	}
	
	
	
	//BUY WEAPON//
	@GetMapping("/store/buy/{name}/{numShipSelected}/{isVertical}")
	public String buy(Model model, @PathVariable String name, @PathVariable int numShipSelected, @PathVariable boolean isVertical) {
		
		//1) Set buy state for weapon
		Weapon weapon = weaponsInDB.findByName(name);
		weapon.setBuy(true);
		
		//2) Save it
		weaponsInDB.save(weapon);
		
		return "redirect:/game/store/" + numShipSelected + '/' + isVertical;
	}
	
	
	
	//SELL WEAPON//
	@GetMapping("/store/sell/{name}/{numShipSelected}/{isVertical}")
	public String sell(Model model, @PathVariable String name, @PathVariable int numShipSelected, @PathVariable boolean isVertical) {
		
		//1) Set sell state for weapon
		Weapon weapon = weaponsInDB.findByName(name);
		weapon.setBuy(false);
		
		//2) Save it
		weaponsInDB.save(weapon);
		
		return "redirect:/game/store/" + numShipSelected + '/' + isVertical;
	}
}
