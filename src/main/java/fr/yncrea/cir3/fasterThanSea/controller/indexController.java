package fr.yncrea.cir3.fasterThanSea.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import fr.yncrea.cir3.fasterThanSea.repository.GameRepository;

@Controller
public class indexController {
	
	@Autowired
	private GameRepository gameInDb;

	@GetMapping("/")
	public String index(Model model) {
		
		if(!gameInDb.findAll().isEmpty()) {
			model.addAttribute("scores", gameInDb.findAll());
		}
		else {
			model.addAttribute("scores", null);
		}
		
		return "index";
	}
	
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
}