package fr.yncrea.cir3.fasterThanSea.dto;

import java.io.Serializable;

public class UserForm implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = new String(password);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = new String (username);
	}
	
	
}
