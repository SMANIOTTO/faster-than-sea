package fr.yncrea.cir3.fasterThanSea.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SessionDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private AccountDto account = new AccountDto();

	public AccountDto getAccount() {
		return account;
	}

	public void setAccount(AccountDto account) {
		this.account = account;
	}

	

	
}

