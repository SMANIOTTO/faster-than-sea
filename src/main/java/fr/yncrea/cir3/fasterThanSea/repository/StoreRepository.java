package fr.yncrea.cir3.fasterThanSea.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.yncrea.cir3.fasterThanSea.domain.Square;
import fr.yncrea.cir3.fasterThanSea.domain.Store;
import fr.yncrea.cir3.fasterThanSea.domain.Weapon;

public interface StoreRepository extends JpaRepository<Store, Long> {

	
}
