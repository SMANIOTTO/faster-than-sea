package fr.yncrea.cir3.fasterThanSea.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.yncrea.cir3.fasterThanSea.domain.Histo;

public interface HistoRepository extends JpaRepository<Histo, Long>{
	
	@Query("SELECT h FROM Histo h WHERE h.game.id = :id")
    public ArrayList<Histo> findCurrentHisto(@Param("id") long id);
	
}