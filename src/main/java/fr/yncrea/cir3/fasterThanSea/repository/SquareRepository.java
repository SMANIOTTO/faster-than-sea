package fr.yncrea.cir3.fasterThanSea.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.yncrea.cir3.fasterThanSea.domain.Square;

public interface SquareRepository extends JpaRepository<Square, Long> {

	@Query("SELECT s FROM Square s WHERE s.player.id = :id ORDER BY s.x, s.y")
    public List<Square> findSquaresCrescentByPlayerId(@Param("id") long id);
	
	@Query("SELECT s FROM Square s WHERE s.x = :x AND s.y = :y AND s.player.id = :id")
    public Square findSquareByCoordAndPlayerId(@Param("x") int x, @Param("y") int y, @Param("id") long id);
	
	@Query("SELECT s FROM Square s WHERE s.id = :id")
    public Square findSquareById(@Param("id") long id);
	
	@Query("SELECT s FROM Square s WHERE (s.state = 'e' OR s.state = 'f') AND s.player.id = :id")
    public List<Square> findSquareRemaining(@Param("id") long id);
	
}
