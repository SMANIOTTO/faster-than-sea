package fr.yncrea.cir3.fasterThanSea.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.yncrea.cir3.fasterThanSea.domain.Game;

public interface GameRepository extends JpaRepository<Game, Long>{
	
	@Query("SELECT g FROM Game g WHERE g.isFinish = FALSE")
    public Game findCurrentGame();
	
}
