package fr.yncrea.cir3.fasterThanSea.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.yncrea.cir3.fasterThanSea.domain.Boat;
public interface BoatRepository extends JpaRepository<Boat, Long>{
	
	@Query("SELECT b FROM Boat b WHERE b.player.id = :id ORDER BY b.type")
    public List<Boat> findBoatsCrescentByPlayerId(@Param("id") long id);
	
	@Query("SELECT b FROM Boat b WHERE b.id = :id")
    public Boat findBoatById(@Param("id") long id);
	
}
