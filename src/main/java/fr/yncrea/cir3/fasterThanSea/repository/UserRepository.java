package fr.yncrea.cir3.fasterThanSea.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import fr.yncrea.cir3.fasterThanSea.service.User;

@Service
public interface UserRepository extends CrudRepository<User, Long> {
	
	public User findByUsername(String username);
	
	 public long count();;
}
