package fr.yncrea.cir3.fasterThanSea.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.yncrea.cir3.fasterThanSea.domain.Player;

public interface PlayerRepository extends JpaRepository<Player, Long>{

	 @Query("SELECT p FROM Player p WHERE p.isCurrent = TRUE AND p.isUser = TRUE")
	    public Player findCurrentPlayer();
	 
	 @Query("SELECT p FROM Player p WHERE p.isCurrent = TRUE AND p.isUser = FALSE")
	    public Player findCurrentEnnemy();
}
