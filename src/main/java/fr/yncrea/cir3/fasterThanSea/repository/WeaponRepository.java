package fr.yncrea.cir3.fasterThanSea.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.yncrea.cir3.fasterThanSea.domain.Weapon;

public interface WeaponRepository extends JpaRepository<Weapon, Long> {

	@Query("SELECT w FROM Weapon w ORDER BY w.id")
    public List<Weapon> findWeaponCrescent();
	
	@Query("SELECT w FROM Weapon w WHERE w.name = :name")
    public Weapon findByName(@Param("name") String name);
	
	@Query("SELECT w FROM Weapon w WHERE w.boat.id = :boatId")
    public List<Weapon> findByBoatId(@Param("boatId") long boatId);
}
